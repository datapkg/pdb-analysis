# pdb-analysis

[![conda](https://img.shields.io/conda/dn/ostrokach-forge/pdb-analysis.svg)](https://anaconda.org/ostrokach-forge/pdb-analysis/)
[![docs](https://img.shields.io/badge/docs-v0.1.0-blue.svg)](https://datapkg.gitlab.io/pdb-analysis/v0.1.0/)
[![pipeline status](https://gitlab.com/datapkg/pdb-analysis/badges/v0.1.0/pipeline.svg)](https://gitlab.com/datapkg/pdb-analysis/commits/v0.1.0/)
[![coverage report](https://gitlab.com/datapkg/pdb-analysis/badges/v0.1.0/coverage.svg)](https://datapkg.gitlab.io/pdb-analysis/v0.1.0/htmlcov/)

Analysis of intra- and inter-chain interactions for proteins in the Protein Data Bank (PDB).
