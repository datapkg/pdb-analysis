import pytest

import pdb_analysis


@pytest.mark.parametrize("attribute", ["__version__"])
def test_attribute(attribute):
    assert getattr(pdb_analysis, attribute)


def test_main():
    import pdb_analysis

    assert pdb_analysis
